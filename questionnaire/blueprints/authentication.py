import functools

from flask import (
    Blueprint,
    current_app,
    flash,
    g,
    redirect,
    render_template,
    request,
    session,
    url_for,
)

from questionnaire import db

from questionnaire.forms.authentication_forms import (
    RegistrationForm,
    LoginForm,
    PasswordResetForm,
    PasswordChangeForm,
    GetUsernameForm,
)

from questionnaire.models import Student

bp = Blueprint("authentication", __name__, url_prefix="/auth")


@bp.before_app_request
def load_logged_in_user():
    student_id = session.get("student_id")

    if not student_id:
        g.student = None
    else:
        g.student = Student.query.filter_by(id=student_id).first()


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not g.student:
            return redirect(url_for("authentication.login"))
        return view(**kwargs)

    return wrapped_view


@bp.route("/register", methods=("GET", "POST"))
def register():
    form = RegistrationForm(request.form)
    if form.validate_on_submit():
        username = form.username.data
        if Student.query.filter_by(username=username).first():
            flash(
                f"Username {username} has been taken. Please try a different username.",
                "danger",
            )
        else:
            db.session.add(
                Student(
                    first_name=form.first_name.data.lower(),
                    last_name=form.last_name.data.lower(),
                    student_id_last_three_digits=form.student_id_last_three_digits.data,
                    username=username,
                    password_hash=Student.hash_password(form.password.data),
                    admin=False,
                )
            )
            db.session.commit()
            flash(
                f"Registration successful. You may log in with your username: {username}.",
                "success",
            )
            return redirect(url_for("authentication.login"))

    return render_template("login_views/register.html", form=form)


@bp.route("/login", methods=("GET", "POST"))
def login():
    form = LoginForm(request.form)
    if form.validate_on_submit():
        student = Student.query.filter_by(username=form.username.data).first()

        if not student or not student.check_password(form.password.data):
            flash("Incorrect password or username does not exist", "danger")

        else:
            session.clear()
            session["student_id"] = student.id
            return redirect(url_for("questionnaire.questionnaire_list"))

    return render_template("login_views/login.html", form=form)


@bp.route("/password-reset", methods=("GET", "POST"))
def password_reset():
    form = PasswordResetForm(request.form)
    if form.validate_on_submit():
        student = Student.query.filter_by(
            username=form.username.data,
            student_id_last_three_digits=form.student_id_last_three_digits.data,
        ).first()

        if not student:
            flash("No user found with username or student ID provided.", "danger")
        else:
            student.password_hash = Student.hash_password(form.password.data)
            db.session.commit()
            flash(
                "Password was changed successfully. Please login with your new password.",
                "success",
            )
            return redirect(url_for("authentication.login"))

    return render_template("login_views/password_reset.html", form=form)


@bp.route("/password-change", methods=("GET", "POST"))
@login_required
def password_change():
    form = PasswordChangeForm(request.form)
    if form.validate_on_submit():
        g.student.password_hash = Student.hash_password(form.password.data)
        db.session.commit()
        flash("Password successfully changed", "success")
        return redirect(url_for("questionnaire.questionnaire_list"))

    return render_template("login_views/password_reset.html", form=form)


@bp.route("/get-username", methods=("GET", "POST"))
def get_username():
    form = GetUsernameForm(request.form)

    if form.validate_on_submit():
        student = Student.query.filter_by(
            first_name=form.first_name.data.lower(),
            last_name=form.last_name.data.lower(),
            student_id_last_three_digits=form.student_id_last_three_digits.data,
        ).first()

        if not student:
            flash("No username found with the name and student ID provided.", "danger")
        else:
            flash(f"Your username is: {student.username}", "primary")

    return render_template("login_views/get_username.html", form=form)


@bp.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("index.index"))
