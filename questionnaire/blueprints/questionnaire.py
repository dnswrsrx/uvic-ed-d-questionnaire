from flask import Blueprint, g, render_template, request, session
import itertools
from questionnaire import db
from questionnaire.blueprints.authentication import login_required
from questionnaire.models import Instrument, FormHeader, TutorialTopic
import random

bp = Blueprint("questionnaire", __name__)


@bp.route("/questionnaire-list")
@login_required
def questionnaire_list():
    order = ("s7_meta", "s6_cog", "s1_learn", "s4_beh", "s5_socemo", "s2_rlq")
    topics = (
        TutorialTopic.query.filter_by(code=topic_code).first()
        for topic_code in order
    )
    return render_template("questionnaire_list.html", topics=topics)


@bp.route("/questionnaire/<tutorial_topic_code>")
@login_required
def questionnaire(tutorial_topic_code):
    # Checks if topic code is valid or returns 404 if code not found
    TutorialTopic.query.filter_by(code=tutorial_topic_code).first_or_404()

    session["tutorial_topic_code"] = tutorial_topic_code

    return render_template(
        "questionnaire.html",
        title=TutorialTopic.query.filter_by(code=tutorial_topic_code)
        .first()
        .topic,
        formheaders_and_instruments=formheaders_and_instruments(
            formheaders_with(tutorial_topic_code)
        ),
    )


def formheaders_and_instruments(formheaders):
    return {
        formheader: section_by_formheader(formheader)
        for formheader in formheaders
    }


def section_by_formheader(formheader):
    instruments_of_formheader = (
        FormHeader.query.filter_by(code=formheader.code).first().instruments
    )

    if formheader.tutorial_topic_code == "s2_rlq":
        yield instrument_view_object(instruments_of_formheader)
    else:
        unique_instrument_names = set(
            map(lambda x: x.name, instruments_of_formheader)
        )
        for name in unique_instrument_names:
            instruments_by_name = Instrument.query.filter_by(name=name).all()
            yield instrument_view_object(instruments_by_name)


def instrument_view_object(instruments):
    return {
        "response_type": instruments[0].response_type,
        "code": instruments[0].code,
        "likert_min": instruments[0].likert_min,
        "likert_min_label": instruments[0].likert_min_label,
        "likert_max": instruments[0].likert_max,
        "likert_max_label": instruments[0].likert_max_label,
        "likert_intermediate_labels": list_of_intermediate_labels(
            instruments[0]
        ),
        "questions": questions_from_instruments(instruments),
    }


def questions_from_instruments(instruments):
    questions = list(
        itertools.chain.from_iterable(
            instrument.questions for instrument in instruments
        )
    )

    if instruments[0].randomise:
        random.shuffle(questions)

    return questions


def formheaders_with(tutorial_topic_code):
    return (
        TutorialTopic.query.filter_by(code=tutorial_topic_code)
        .first()
        .form_headers
    )


def list_of_intermediate_labels(instrument):
    if instrument.likert_intermediate_labels:
        return instrument.likert_intermediate_labels.split(", ")
    return None
