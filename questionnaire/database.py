import click
import csv, os
from flask import current_app
from flask.cli import with_appcontext
from questionnaire import db
from questionnaire.models import Question, Instrument, FormHeader, TutorialTopic


@click.command("init-db")
@with_appcontext
def init_database():
    populate_tutorial_topic()
    populate_form_headers()
    populate_instruments()
    populate_question_bank()
    db.session.commit()


def populate_instruments():
    csv_file = os.path.join(current_app.instance_path, "instruments.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        next(table)
        for row in table:
            if not Instrument.query.filter_by(code=type_csv_data(row[0])).all():
                db.session.add(
                    Instrument(
                        **{
                            "code": type_csv_data(row[0]),
                            "name": type_csv_data(row[1]),
                            "subfactor": type_csv_data(row[2]),
                            "randomise": type_csv_data(row[3]),
                            "response_type": type_csv_data(row[4]),
                            "likert_min": type_csv_data(row[5]),
                            "likert_min_label": type_csv_data(row[6]),
                            "likert_max": type_csv_data(row[7]),
                            "likert_max_label": type_csv_data(row[8]),
                            "likert_intermediate_labels": type_csv_data(row[9]),
                            "scoring_method": type_csv_data(row[10]),
                            "form_header_code": type_csv_data(row[11]),
                            "number_of_questions": type_csv_data(row[12]),
                            "report_description": type_csv_data(row[13]),
                            "tutorial_topic_code": type_csv_data(row[14]),
                        }
                    )
                )


def populate_form_headers():
    csv_file = os.path.join(current_app.instance_path, "form_headers.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        next(table)
        for row in table:
            if not FormHeader.query.filter_by(code=type_csv_data(row[0])).all():
                db.session.add(
                    FormHeader(
                        **{
                            "code": type_csv_data(row[0]),
                            "header": type_csv_data(row[1]),
                            "instruction": type_csv_data(row[2]),
                            "tutorial_topic_code": type_csv_data(row[3]),
                        }
                    )
                )


def populate_question_bank():
    csv_file = os.path.join(current_app.instance_path, "question_bank.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        next(table)
        for row in table:
            if not Question.query.filter_by(code=type_csv_data(row[0])).all():
                db.session.add(
                    Question(
                        **{
                            "code": type_csv_data(row[0]),
                            "statement": type_csv_data(row[1]),
                            "reversed_score": type_csv_data(row[2]),
                            "instrument_code": type_csv_data(row[3]),
                        }
                    )
                )


def populate_tutorial_topic():
    csv_file = os.path.join(current_app.instance_path, "tutorial_topics.csv")
    with open(csv_file, "r", encoding="utf-8") as content:
        table = csv.reader(content)
        next(table)
        for row in table:
            if not TutorialTopic.query.filter_by(
                code=type_csv_data(row[0])
            ).all():
                db.session.add(
                    TutorialTopic(
                        **{
                            "code": type_csv_data(row[0]),
                            "topic": type_csv_data(row[1]),
                        }
                    )
                )


def type_csv_data(data):
    if data.isdigit():
        return int(data)
    elif data.lower() == "false":
        return False
    elif data.lower() == "true":
        return True
    elif data.lower() == "none":
        return None
    elif data.lower() == "null":
        return None
    else:
        return data
