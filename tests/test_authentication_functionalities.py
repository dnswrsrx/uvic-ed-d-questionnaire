import unittest
from test_questionnaire_app_instantiation import BaseTestCase
from questionnaire.models import Student, TutorialTopic


class AccessQuestionnaireNotLoggedInTest(BaseTestCase):
    """ Checks responses of getting endpoints while not logged in.

    Four pages should be accessible while essentially everything else requires
    logging in.

    """

    # Tests to see if can access register and login pages.
    def test_register_page(self):
        response = self.app.get("/auth/register", follow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.location, None)
        self.assertIn(b"Register</title>", response.data)

    def test_login_page(self):
        response = self.app.get("/auth/login", follow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.location, None)
        self.assertIn(b"Log In</title>", response.data)

    def test_get_username_page(self):
        response = self.app.get("/auth/get-username", follow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.location, None)
        self.assertIn(b"Get Username</title>", response.data)

    def test_password_reset_page(self):
        response = self.app.get("/auth/password-reset", follow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.location, None)
        self.assertIn(b"Password Reset</title>", response.data)

    # Tests to see if can access login_required pages.
    # Code 302 expected and /auth/login should be in response location url
    def test_password_change_not_logged_in(self):
        response = self.app.get("/auth/password-change", follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertIn("/auth/login", response.location)

    def test_questionnaire_list_not_logged_in(self):
        response = self.app.get("/questionnaire-list", follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertIn("/auth/login", response.location)

    def test_questionnaire_not_logged_in(self):
        for topic in TutorialTopic.query.all():
            with self.subTest(code=topic.code):
                response = self.app.get(
                    f"/questionnaire/{topic.code}", follow_redirects=False
                )
                self.assertEqual(response.status_code, 302)
                self.assertIn("/auth/login", response.location)

    def test_responses_not_logged_in(self):
        response = self.app.get("/responses", follow_redirects=False)
        self.assertEqual(response.status_code, 302)
        self.assertIn("/auth/login", response.location)


class RegisteringLoggingInAndLoggingOutTest(BaseTestCase):
    """ Tests to make sure registration, logging in, and logging out works.

    Also checks if re-registration is possible: it should not be!
    """

    @classmethod
    def setUpClass(cls):
        """ Extends inherited class method with a test_user containing
        registration details. The object's keys match what the forms require.
        """
        super().setUpClass()

        cls.test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

    def tearDown(self):
        Student.query.filter_by(username="test").delete()
        self.db.session.commit()

    def test_user_registration(self):
        """
        There should be a redirect response to the login page after
        registration. Registered user should be inserted into the database.
        """

        student_table_before_registration = Student.query.all()
        self.assertEqual(len(student_table_before_registration), 0)

        response = self.app.post(
            "/auth/register", data=self.test_user, follow_redirects=False
        )

        self.assertEqual(response.status_code, 302)
        self.assertIn("/auth/login", response.location)

        student_table_after_registration = Student.query.all()
        self.assertEqual(len(student_table_after_registration), 1)
        self.assertEqual(student_table_after_registration[0].username, "test")

    def test_user_reregistration(self):
        """
        Same username should not be able to re-register. Error message should
        display (Username <username> has been taken. Please try a different
        username.) and header should be the registration page header
        (<h1>Register</h1>).
        """

        student_table_before_registration = Student.query.all()
        self.assertEqual(len(student_table_before_registration), 0)

        self.app.post("/auth/register", data=self.test_user)

        student_table_after_registration = Student.query.all()
        self.assertEqual(len(student_table_after_registration), 1)

        response = self.app.post(
            "/auth/register", data=self.test_user, follow_redirects=True
        )

        student_table_after_reregistration = Student.query.all()
        self.assertEqual(len(student_table_after_reregistration), 1)

        self.assertIn(b"Register</title>", response.data)
        self.assertIn(
            b"Username %b has been taken. Please try a different username."
            % self.test_user["username"].encode("utf8"),
            response.data,
        )

    def test_registering_and_logging_in(self):
        """
        Once student is registered and logged in, we should be redirected to
        questionnaire list.
        """

        self.app.post("/auth/register", data=self.test_user, follow_redirects=False)

        non_redirected_response = self.app.post(
            "/auth/login", data=self.test_user, follow_redirects=False
        )
        self.assertEqual(len(Student.query.all()), 1)
        self.assertEqual(non_redirected_response.status_code, 302)
        self.assertIn("/questionnaire-list", non_redirected_response.location)

        redirected_response = self.app.post(
            "/auth/login", data=self.test_user, follow_redirects=True
        )
        self.assertEqual(len(Student.query.all()), 1)
        self.assertEqual(redirected_response.status_code, 200)
        self.assertIn(b"Questionnaire List</title>", redirected_response.data)

    def test_registering_logging_in_and_logging_out(self):
        """
        Once student is registered, logged in, and logged out, we should be
        redirected to index.
        """

        self.app.post("/auth/register", data=self.test_user)
        self.app.post("/auth/login", data=self.test_user)

        non_redirected_logout_response = self.app.get(
            "/auth/logout", follow_redirects=False
        )

        self.assertEqual(non_redirected_logout_response.status_code, 302)

        self.app.post("/auth/login", data=self.test_user)
        redirected_logout_response = self.app.get("/auth/logout", follow_redirects=True)

        self.assertEqual(redirected_logout_response.status_code, 200)
        self.assertIn(b"Home Page</title>", redirected_logout_response.data)
        self.assertIn(
            b"Welcome to the site for UVic ED-D 101 surveys!",
            redirected_logout_response.data,
        )


class AccessQuestionnaireLoggedInTest(BaseTestCase):
    """Tests whether viewing pages where login is required is possible after
    logging in.
    """

    @classmethod
    def setUpClass(cls):
        """ Test user instance created and is registered and logged in.
        """

        super().setUpClass()

        cls.test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

        cls.app.post("/auth/register", data=cls.test_user)
        cls.app.post("/auth/login", data=cls.test_user)

    # Tests to see if can access login_required pages.
    def test_password_change_logged_in(self):
        response = self.app.get("/auth/password-change", follow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Password Reset</title>", response.data)

    def test_questionnaire_list_logged_in(self):
        response = self.app.get("/questionnaire-list", follow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Questionnaire List</title>", response.data)

    def test_questionnaire_logged_in(self):
        for topic in TutorialTopic.query.all():
            with self.subTest(code=topic.code):
                response = self.app.get(
                    f"/questionnaire/{topic.code}", follow_redirects=False
                )
                self.assertEqual(response.status_code, 200)
                self.assertIn(
                    bytes(
                        TutorialTopic.query.filter_by(code=topic.code).first().topic,
                        "utf-8",
                    ),
                    response.data,
                )

    def test_responses_logged_in(self):
        response = self.app.get("/responses", follow_redirects=False)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Responses</title>", response.data)


class ChangingOrGettingLoginDetailsTest(BaseTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.test_user = {
            "username": "test",
            "first_name": "First",
            "last_name": "Last",
            "student_id_last_three_digits": "098",
            "password": "unhashedpassword",
            "password_confirm": "unhashedpassword",
        }

    def setUp(self):
        self.app.post("/auth/register", data=self.test_user)

    def tearDown(self):
        Student.query.filter_by(username="test").delete()
        self.db.session.commit()

    def test_get_username(self):
        """
        There should be the text "Your username is: test", probably wrapped with
        HTML tags and new lines and tabs.
        """
        response = self.app.post(
            "/auth/get-username", data=self.test_user, follow_redirects=False
        )

        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Your username is: test", response.data)

    def test_forgot_password(self):
        """
        After resetting password, should redirect to login. When logged in with
        new password, shold redirect to list of questionnaire. When logging in
        with old password, should not have redirect and should still be in Log
        In page with the flash incorrect password.
        """
        data = {
            "username": self.test_user["username"],
            "student_id_last_three_digits": self.test_user[
                "student_id_last_three_digits"
            ],
            "password": "newunhashedpassword",
            "password_confirm": "newunhashedpassword",
        }

        reset_response = self.app.post(
            "/auth/password-reset", data=data, follow_redirects=False
        )

        self.assertEqual(reset_response.status_code, 302)
        self.assertIn("/auth/login", reset_response.location)

        login_response_with_new_password = self.app.post(
            "/auth/login", data=data, follow_redirects=False
        )

        self.assertEqual(login_response_with_new_password.status_code, 302)
        self.assertIn("questionnaire-list", login_response_with_new_password.location)

        self.app.get("/auth/logout")

        login_response_with_old_password = self.app.post(
            "/auth/login", data=self.test_user, follow_redirects=False
        )

        self.assertNotEqual(login_response_with_old_password.status_code, 302)
        self.assertIn(b"Log In</title>", login_response_with_old_password.data)
        self.assertIn(b"Incorrect password", login_response_with_old_password.data)

    def test_change_password(self):
        """
        After changing password, there should be a redirect to the list of
        questionnaires. Logging in with the old password should keep us in the
        login page with the wrong password error flashed.
        """
        data = {
            "username": self.test_user["username"],
            "password": "newunhashedpassword",
            "password_confirm": "newunhashedpassword",
        }

        self.app.post("/auth/login", data=self.test_user)

        changed_password_response = self.app.post(
            "/auth/password-change", data=data, follow_redirects=False
        )

        self.assertEqual(changed_password_response.status_code, 302)
        self.assertIn("questionnaire-list", changed_password_response.location)

        self.app.get("/auth/logout")

        login_response_with_old_password = self.app.post(
            "/auth/login", data=self.test_user, follow_redirects=False
        )

        self.assertIn(b"Log In</title>", login_response_with_old_password.data)
        self.assertIn(b"Incorrect password", login_response_with_old_password.data)

        login_response_with_new_password = self.app.post(
            "/auth/login", data=data, follow_redirects=False
        )

        self.assertEqual(login_response_with_new_password.status_code, 302)
        self.assertIn("questionnaire-list", login_response_with_new_password.location)
