import unittest
import os, sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from questionnaire import create_app, db
from questionnaire.models import Question, Instrument, FormHeader, TutorialTopic


def create_test_app():
    app = create_app()
    app.config["TESTING"] = True
    app.config["DEBUG"] = False
    app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = "postgresql:///uvic-ed-d-questionnaire-db"
    return app


class DatabaseContentTests(unittest.TestCase):
    """ Tests for the content of the Question, Instrument, FormHeader, and
    TutorialTopic tables.

    These tables are pre-populated and should remain stable throughout the
    database's existence.
    """

    # Set up application and context to test database
    @classmethod
    def setUpClass(cls):
        cls.app = create_test_app()
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.app = cls.app.test_client()

    @classmethod
    def tearDownClass(cls):
        cls.app_context.pop()

    # Tests whether an instance is running. Context required for testing.
    def test_main_page(self):
        response = self.app.get("/", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_number_of_tutorialtopic(self):
        self.assertEqual(len(TutorialTopic.query.all()), 6)

    def test_number_of_formheaders(self):
        form_headers = [
            formheader.tutorial_topic_code
            for formheader in FormHeader.query.all()
        ]

        self.assertEqual(form_headers.count("s1_learn"), 4)
        self.assertEqual(form_headers.count("s2_rlq"), 4)
        self.assertEqual(form_headers.count("s4_beh"), 4)
        self.assertEqual(form_headers.count("s5_socemo"), 2)
        self.assertEqual(form_headers.count("s6_cog"), 1)
        self.assertEqual(form_headers.count("s7_meta"), 1)

    def test_number_of_instruments(self):
        instruments = [
            instrument.tutorial_topic_code
            for instrument in Instrument.query.all()
        ]

        self.assertEqual(instruments.count("s1_learn"), 10)
        self.assertEqual(instruments.count("s2_rlq"), 18)
        self.assertEqual(instruments.count("s4_beh"), 5)
        self.assertEqual(instruments.count("s6_cog"), 4)
        self.assertEqual(instruments.count("s7_meta"), 8)

    def test_number_of_randomise_in_instruments(self):
        randomise = [
            instrument.tutorial_topic_code
            for instrument in Instrument.query.filter_by(randomise=True).all()
        ]

        self.assertEqual(randomise.count("s1_learn"), 6)
        self.assertEqual(randomise.count("s2_rlq"), 18)
        self.assertEqual(randomise.count("s4_beh"), 3)
        self.assertEqual(randomise.count("s5_socemo"), 0)
        self.assertEqual(randomise.count("s6_cog"), 0)
        self.assertEqual(randomise.count("s7_meta"), 0)

    def test_number_of_likert_in_instruments(self):
        likert = [
            instrument.tutorial_topic_code
            for instrument in Instrument.query.filter_by(
                response_type="likert"
            ).all()
        ]

        self.assertEqual(likert.count("s1_learn"), 10)
        self.assertEqual(likert.count("s2_rlq"), 12)
        self.assertEqual(likert.count("s4_beh"), 5)
        self.assertEqual(likert.count("s5_socemo"), 4)
        self.assertEqual(likert.count("s6_cog"), 4)
        self.assertEqual(likert.count("s7_meta"), 0)

    def test_number_of_checkbox_in_instruments(self):
        checkbox = [
            instrument.tutorial_topic_code
            for instrument in Instrument.query.filter_by(
                response_type="checkbox"
            ).all()
        ]

        self.assertEqual(checkbox.count("s1_learn"), 0)
        self.assertEqual(checkbox.count("s2_rlq"), 6)
        self.assertEqual(checkbox.count("s4_beh"), 0)
        self.assertEqual(checkbox.count("s5_socemo"), 0)
        self.assertEqual(checkbox.count("s6_cog"), 0)
        self.assertEqual(checkbox.count("s7_meta"), 0)

    def test_number_of_instruments_in_headers(self):
        instrument_and_code = [
            (instrument.tutorial_topic_code, instrument.form_header_code)
            for instrument in Instrument.query.all()
        ]

        self.assertEqual(instrument_and_code.count(("s1_learn", "s1-1")), 1)
        self.assertEqual(instrument_and_code.count(("s1_learn", "s1-2")), 2)
        self.assertEqual(instrument_and_code.count(("s1_learn", "s1-3")), 6)
        self.assertEqual(instrument_and_code.count(("s1_learn", "s1-4")), 1)
        self.assertEqual(instrument_and_code.count(("s2_rlq", "s2-1")), 6)
        self.assertEqual(instrument_and_code.count(("s2_rlq", "s2-2")), 2)
        self.assertEqual(instrument_and_code.count(("s2_rlq", "s2-3")), 4)
        self.assertEqual(instrument_and_code.count(("s2_rlq", "s2-4")), 6)
        self.assertEqual(instrument_and_code.count(("s4_beh", "s4-1")), 1)
        self.assertEqual(instrument_and_code.count(("s4_beh", "s4-2")), 1)
        self.assertEqual(instrument_and_code.count(("s4_beh", "s4-3")), 1)
        self.assertEqual(instrument_and_code.count(("s4_beh", "s4-4")), 2)
        self.assertEqual(instrument_and_code.count(("s5_socemo", "s5-1")), 1)
        self.assertEqual(instrument_and_code.count(("s5_socemo", "s5-2")), 3)
        self.assertEqual(instrument_and_code.count(("s6_cog", "s6-1")), 4)
        self.assertEqual(instrument_and_code.count(("s7_meta", "s7-1")), 8)

    def test_number_of_truefalse_in_instruments(self):
        truefalse = [
            instrument.tutorial_topic_code
            for instrument in Instrument.query.filter_by(
                response_type="truefalse"
            ).all()
        ]

        self.assertEqual(truefalse.count("s1_learn"), 0)
        self.assertEqual(truefalse.count("s2_rlq"), 0)
        self.assertEqual(truefalse.count("s4_beh"), 0)
        self.assertEqual(truefalse.count("s5_socemo"), 0)
        self.assertEqual(truefalse.count("s6_cog"), 0)
        self.assertEqual(truefalse.count("s7_meta"), 8)

    def test_number_of_scoring_method_in_instruments(self):
        scoring_methods = [
            instrument.scoring_method for instrument in Instrument.query.all()
        ]

        self.assertEqual(scoring_methods.count("average"), 48)
        self.assertEqual(scoring_methods.count(None), 1)

    def test_number_of_instruments_with_intermediate_labels(self):
        likert_intermediate_labels = [
            instrument.likert_intermediate_labels
            for instrument in Instrument.query.all()
        ]

        self.assertEqual(likert_intermediate_labels.count(None), 42)
        self.assertEqual(len(likert_intermediate_labels) - 42, 7)

    def test_number_of_likert_max_label_in_instruments(self):
        labels = [
            instrument.likert_max_label for instrument in Instrument.query.all()
        ]

        self.assertEqual(len(set(labels)), 8)
        self.assertEqual(labels.count("strongly agree"), 14)
        self.assertEqual(labels.count("very true for me this week"), 6)
        self.assertEqual(labels.count("very confident"), 1)
        self.assertEqual(labels.count(None), 14)
        self.assertEqual(labels.count("extremely accurate/successful"), 6)
        self.assertEqual(labels.count("always or always almost true of me"), 4)
        self.assertEqual(labels.count("every day"), 3)
        self.assertEqual(labels.count("very true"), 1)

    def test_number_of_likert_max(self):
        max = [instrument.likert_max for instrument in Instrument.query.all()]

        self.assertEqual(max.count(5), 32)
        self.assertEqual(max.count(None), 14)
        self.assertEqual(max.count(7), 3)

    def test_number_of_likert_min_label_in_instruments(self):
        labels = [
            instrument.likert_min_label for instrument in Instrument.query.all()
        ]

        self.assertEqual(len(set(labels)), 8)
        self.assertEqual(labels.count("strongly disagree"), 14)
        self.assertEqual(labels.count("not at all true for me this week"), 6)
        self.assertEqual(labels.count("not confident"), 1)
        self.assertEqual(labels.count(None), 14)
        self.assertEqual(labels.count("not at all accurate/successful"), 6)
        self.assertEqual(labels.count("never or rarely true of me"), 4)
        self.assertEqual(labels.count("never"), 3)
        self.assertEqual(labels.count("not at all true"), 1)

    def test_number_of_likert_min(self):
        min = [instrument.likert_min for instrument in Instrument.query.all()]

        self.assertEqual(min.count(1), 35)
        self.assertEqual(min.count(None), 14)

    def test_number_of_instrument_names(self):
        names = [instrument.name for instrument in Instrument.query.all()]

        self.assertEqual(len(names), 49)
        self.assertEqual(len(set(names)), 16)

    def test_number_of_questions(self):
        instrument_codes = [
            instrument.code for instrument in Instrument.query.all()
        ]

        self.assertEqual(len(Question.query.all()), 247)
        # Test for the number of questions per instrument.
        # Compares number of question by instrument code, and number of question
        # found in instrument table
        for code in instrument_codes:
            with self.subTest(code=code):
                self.assertEqual(
                    len(Question.query.filter_by(instrument_code=code).all()),
                    Instrument.query.filter_by(code=code)
                    .first()
                    .number_of_questions,
                )

    def test_number_of_reversed_scored_questions(self):
        reversed_scores = [
            question.instrument.code
            for question in Question.query.filter_by(reversed_score=True).all()
        ]

        self.assertEqual(reversed_scores.count("gm"), 4)
        self.assertEqual(reversed_scores.count("c"), 4)
        self.assertEqual(reversed_scores.count("g"), 4)
        self.assertEqual(reversed_scores.count("uv"), 0)
        self.assertEqual(reversed_scores.count("map"), 0)
        self.assertEqual(reversed_scores.count("mav"), 0)
        self.assertEqual(reversed_scores.count("pap"), 0)
        self.assertEqual(reversed_scores.count("pav"), 0)
        self.assertEqual(reversed_scores.count("ta"), 0)
        self.assertEqual(reversed_scores.count("sesrl"), 0)
        for code in reversed_scores:
            with self.subTest(code=code):
                self.assertNotIn("rlq-", code)
        for code in reversed_scores:
            with self.subTest(code=code):
                self.assertNotIn("ma-", code)
        for code in reversed_scores:
            with self.subTest(code=code):
                self.assertNotIn("sp-", code)
        self.assertEqual(reversed_scores.count("sb"), 5)
        for code in reversed_scores:
            with self.subTest(code=code):
                self.assertNotIn("mh-", code)
        self.assertEqual(reversed_scores.count("ap"), 12)
        self.assertEqual(reversed_scores.count("proc"), 0)
        self.assertEqual(reversed_scores.count("env"), 0)
        self.assertEqual(reversed_scores.count("tmp"), 0)
        self.assertEqual(reversed_scores.count("tmm"), 0)


if __name__ == "__main__":
    unittest.main()
